import base64
import os.path
from io import BytesIO

import requests
from PIL import Image


def image_to_base64(image_path):
    if not os.path.exists(image_path):
        return ""
    i = Image.open(image_path)
    buffered = BytesIO()
    i.save(buffered, format="PNG")
    img_str = base64.b64encode(buffered.getvalue())
    return img_str.decode('utf-8')


def interrogate(image_path):
    b64 = image_to_base64(image_path)
    if b64 == '':
        print("图片不存在")
        return
    payload = {
        "image": b64,
        "model": "wd14-vit-v2",
        "threshold": 0.35,
    }
    rsp = requests.post("http://117.50.194.92:7861/tagger/v1/interrogate", json=payload)
    result = rsp.json()
    print(result)


if __name__ == '__main__':
    image_path = "/Users/user/aigc/pic/tangwei/tw6.png"
    interrogate(image_path)
