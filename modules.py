import json

json_str = '''{"module_list": [
    "none",
    "canny",
    "depth",
    "depth_leres",
    "depth_leres++",
    "hed",
    "hed_safe",
    "mediapipe_face",
    "mlsd",
    "normal_map",
    "openpose",
    "openpose_hand",
    "openpose_face",
    "openpose_faceonly",
    "openpose_full",
    "clip_vision",
    "color",
    "pidinet",
    "pidinet_safe",
    "pidinet_sketch",
    "pidinet_scribble",
    "scribble_xdog",
    "scribble_hed",
    "segmentation",
    "threshold",
    "depth_zoe",
    "normal_bae",
    "oneformer_coco",
    "oneformer_ade20k",
    "lineart",
    "lineart_coarse",
    "lineart_anime",
    "lineart_standard",
    "shuffle",
    "tile_resample",
    "invert",
    "lineart_anime_denoise",
    "reference_only",
    "reference_adain",
    "reference_adain+attn",
    "inpaint",
    "inpaint_only",
    "inpaint_only+lama",
    "tile_colorfix",
    "tile_colorfix+sharp"
  ]}'''


def to_dict(string):
    return json.loads(string)


def to_csv(dict_name):
    content = ''
    modules = dict_name['module_list']
    for module in modules:
        content += f'{module}\n'
    with open('modules.csv', 'w') as f:
        f.write(content)


if __name__ == '__main__':
    json_dict = to_dict(json_str)
    to_csv(json_dict)
