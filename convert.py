from PIL import Image
import os

def is_webp(image_path):
    if not os.path.exists(image_path):
        return False
    ext = os.path.splitext(image_path)[1]
    if ext == '.webp':
        return True
    
def webp_to_png(image_path):
    if not is_webp(image_path):
        return
    im = Image.open(image_path).convert('RGB')
    im.save(image_path.replace('.webp', '.png'))
    print('converting {} to png'.format(image_path))

def batch_webp2png(image_dir):
    # if there is no image_dir, return
    if not os.path.exists(image_dir):
        return
    # if image_dir is a file, convert it
    if os.path.isfile(image_dir):
        webp_to_png(image_dir)
        return
    # if image_dir is a directory, convert all images in it
    print('converting images in {}'.format(image_dir))
    for root, dirs, files in os.walk(image_dir):
        image_path = [os.path.join(root, file) for file in files]
        for path in image_path:
            webp_to_png(path)

if __name__ == '__main__':
    image_dir = "/Users/user/aigc/pic/tangwei"
    batch_webp2png(image_dir)