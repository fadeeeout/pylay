import base64
from io import BytesIO

from PIL import Image
from zopflipng import png_optimize


def compress_image(image_base64: str) -> str:
    data = base64.b64decode(image_base64)
    result, code = png_optimize(data)
    if code == 0:
        return base64.b64encode(result).decode('utf-8')

    return ""


def pil_to_base64(img) -> str:
    buffered = BytesIO()
    img.save(buffered, format="PNG")
    img_str = base64.b64encode(buffered.getvalue())
    return img_str.decode('utf-8')


def base64_to_pil(im_b64) -> Image:
    im_b64 = bytes(im_b64, 'utf-8')
    im_bytes = base64.b64decode(im_b64)  # im_bytes is a binary image
    im_file = BytesIO(im_bytes)  # convert image to file-like object
    img = Image.open(im_file)
    img = img.convert("RGBA")
    return img


def pil_optimize(img: Image) -> Image:
    buffered = BytesIO()
    img.save(buffered, format="PNG", optimize=True)
    buffered.seek(0)
    return Image.open(buffered)


if __name__ == '__main__':
    image_path = "/Users/user/aigc/temp/dog.png"
    img = Image.open(image_path)
    # bimg = pil_to_base64(img)
    # bimg = compress_image(bimg)
    # img = base64_to_pil(bimg)
    img.save("/Users/user/aigc/temp/dog_compressed.png", "PNG", quality=50, optimize=True)
