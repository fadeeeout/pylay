from datetime import datetime
import imagequant
from PIL import Image

speed = 4


def run_pngquant(image_path):
    img = Image.open(image_path)
    output_image = imagequant.quantize_pil_image(
        img,
        dithering_level=1,  # from 0.0 to 1.0
        max_colors=255,  # from 1 to 256
    )
    output_image.save("/Users/user/aigc/temp/test/us3/cat_new.png", format="PNG")


if __name__ == '__main__':
    start = datetime.now()
    image_dir = "/Users/user/aigc/temp/test/us3/cat.png"
    run_pngquant(image_dir)
    elapsed = datetime.now() - start
    print(f'pngquant elapsed: {elapsed}')
