import requests
from util.helper import image_to_base64


def batch_interrogate_payload(image_paths: []):
    return {
        "images": [{"name": path, "data": image_to_base64(path)} for path in image_paths],
        "model": 'wd14-vit-v2',
        "threshold": 0.35,
    }


def batch_interrogate(payload: dict, uri='/batch_interrogate'):
    url = f"http://127.0.0.1:7861/tagger/v1{uri}"

    resp = requests.post(url=url, json=payload)
    if resp.status_code != 200:
        print(f'状态码：{resp.status_code}')
        print(f'错误信息：{resp.json()}')
        return None
    response = resp.json()
    if response['code'] and response['code'] != 200:
        print(f'错误信息：{response["message"]}')
        return None
    return response


def interrogate_test():
    base_dir = "/Users/user/aigc/pic/tangwei"
    images = [
        f'{base_dir}/tw1.png',
        f'{base_dir}/tw2.jpeg',
        f'{base_dir}/tw3.jpeg',
        f'{base_dir}/tw4.jpeg',
    ]
    payload = batch_interrogate_payload(images)
    response = batch_interrogate(payload)
    print(response)


if __name__ == '__main__':
    interrogate_test()
