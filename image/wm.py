from PIL import Image, ImageDraw, ImageFont, ImageFilter
from matplotlib import pyplot as plt

image_path = "/Users/user/aigc/temp/dark.png"

img = Image.open(image_path).convert('RGBA')
width, height = img.size


def get_fontsize(w, h):
    """
    根据图片大小获取字体大小，计算公式: 设字体大小为x, 短边为y, x/y = 10/512
    :param w: 图片宽度
    :param h: 图片高度
    :return: 字体大小
    """
    min_len = min(w, h)
    return ((min_len * 10) / 512) + 1


def get_offset(text_fontsize):
    return text_fontsize * 0.8


content = 'AIGC生成内容'

fontsize = get_fontsize(width, height)

font = ImageFont.truetype('SourceHanSansCN-Normal.otf', fontsize)
content_len = font.getlength(content)
txt_height = font.getbbox(content)[3] - font.getbbox(content)[1]

x = width - content_len - get_offset(fontsize)
y = height - txt_height - get_offset(fontsize)

blurred = Image.new("RGBA", (width, height))
draw = ImageDraw.Draw(blurred)
draw.text((x, y), content, font=font, fill=(0, 0, 0, 40))

blurred = blurred.filter(ImageFilter.BoxBlur(4))

img.paste(blurred, (0, 0), blurred)

draw = ImageDraw.Draw(img)
draw.text((x, y), content, font=font, fill=(245, 245, 245, 200))

# plt.imshow(img)
# plt.show()
img.save("/Users/user/aigc/temp/wm_save.png", "PNG")
