import requests
from util.helper import image_to_base64, save_b64_img


def matting_payload(img, mask, crop_type, text_prompt, box_threshold, text_threshold, iou_threshold, guidance_mode,
                    boxes):
    return {
        "input_image": img,
        "mask": mask,
        "matting_type": crop_type,
        "text_prompt": text_prompt,
        "box_threshold": box_threshold,
        "text_threshold": text_threshold,
        "iou_threshold": iou_threshold,
        "guidance_mode": guidance_mode,
        "boxes": boxes
    }


def do_matting(payload):
    URL = "http://127.0.0.1:7820/matting_anything"
    resp = requests.post(url=URL, json=payload)
    if resp.status_code != 200:
        raise Exception(f'状态码：{resp.status_code}')
    response = resp.json()
    if response['code'] != 200:
        raise Exception(f'错误信息：{response["message"]}')
    return response['images']


def matt():
    image_path = '/Users/user/aigc/temp/portrait.jpeg'
    mask_path = "/Users/user/aigc/temp/pmask1.png"
    payload = matting_payload(
        image_to_base64(image_path),
        # "",
        image_to_base64(mask_path),
        "mask",
        "",
        0.25,
        0.25,
        0.5,
        "alpha",
        []
    )
    res = do_matting(payload)
    idx = 1
    print(len(res))
    for r in res:
        save_b64_img(r, f'/Users/user/aigc/temp/mouse_matting_{idx}.png')
        idx += 1


if __name__ == '__main__':
    matt()
