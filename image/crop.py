import cv2
import numpy as np
from skimage.measure import label, regionprops, find_contours

# 加载原图和黑白蒙版图
original_image = cv2.imread('/Users/user/aigc/temp/mouse.jpeg', cv2.IMREAD_COLOR)
mask_img = cv2.imread('/Users/user/aigc/temp/mul.png', cv2.IMREAD_GRAYSCALE)  # 以灰度模式读取蒙版图


def mask_to_border(mask):
    h, w = mask.shape
    border = np.zeros((h, w))

    contours = find_contours(mask, 128)
    for contour in contours:
        for c in contour:
            x = int(c[0])
            y = int(c[1])
            border[x][y] = 255

    return border


""" Mask to bounding boxes """


def mask_to_bbox(mask):
    bboxes = []

    mask = mask_to_border(mask)
    lbl = label(mask)
    props = regionprops(lbl)
    for prop in props:
        x1 = prop.bbox[1]
        y1 = prop.bbox[0]

        x2 = prop.bbox[3]
        y2 = prop.bbox[2]

        bboxes.append([x1, y1, x2, y2])

    return bboxes


bboxes = mask_to_bbox(mask_img)
for bbox in bboxes:
    x1, y1, x2, y2 = bbox
    cv2.rectangle(original_image, (x1, y1), (x2, y2), (0, 255, 0), 2)

cv2.imwrite("/Users/user/aigc/temp/mouse_bbox.jpg", original_image)
