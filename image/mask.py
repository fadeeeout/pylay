import numpy as np
import requests
from PIL import Image, ImageOps
import cv2
from util.helper import image_to_base64,base64_to_pil


def draw_rect(img, x, y, w, h):
    result = cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
    return result


def is_black_and_white(image):
    arr = np.array(image)
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            r, g, b = arr[i, j, :]
            if r == g and g == b:
                if r != 255 and r != 0:
                    print(r)
            if r != g and g != b:
                return False
    return True

def black_and_white_mask_invert(img: Image) -> Image:
    """
    将黑白蒙版图像反转，黑色变为白色，白色变为黑色
    :param img: PIL.Image.Image 对象
    :return: PIL.Image.Image 对象
    """
    # 将图像转换为带有 alpha 通道的模式
    img = img.convert("L")
    inverted_img = Image.eval(img, lambda x: 255 - x)
    inverted_img = inverted_img.convert("1")
    return inverted_img


if __name__ == '__main__':
    mg_path = "/Users/user/aigc/temp/compare/mask.png"
    # img = cv2.imread(img_path)
    # img = draw_rect(img, 86, 282, 1621, 566)
    # cv2.imwrite("/Users/user/aigc/temp/truck_rect.jpg", img)
    # black_and_white_mask_invert(Image.open(mg_path)).save("/Users/user/aigc/temp/newmask_invert.png", "PNG")
    # img = Image.open(mg_path).convert("RGB")
    # new_img = ImageOps.invert(img)
    # new_img.save("/Users/user/aigc/temp/compare/mask_invert.png", "PNG")
    data = {
        "image": image_to_base64(mg_path),
        "is_revert": True
    }
    response = requests.post(url="http://127.0.0.1:7861/service/v1/mask-convert", json=data)
    if response.status_code != 200:
        print(f'状态码：{response.status_code}')
        print(f'错误信息：{response.json()}')
        exit()
    rsp = response.json()
    if rsp['code'] and rsp['code'] != 200:
        print(f'错误信息：{rsp["message"]}')
        exit()
    img = rsp['converted_image']
    result = base64_to_pil(img)
    result.save("/Users/user/aigc/temp/compare/mask_invert_1.png", "PNG")
    pass
