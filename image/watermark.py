import cv2

# 加载图片
image = cv2.imread('/Users/user/aigc/temp/mouse.jpeg')

# 在图片上添加文字水印
text = 'AIGC生成内容'
position = (50, 50)  # 文字水印的位置
font = cv2.FONT_HERSHEY_SIMPLEX  # 字体类型
font_scale = 1  # 字体缩放因子
font_color = (245, 245, 245)  # 字体颜色 (B, G, R)
line_type = cv2.LINE_AA  # 线型

# 绘制阴影效果
shadow_color = (0, 0, 0)  # 阴影颜色 (B, G, R)
shadow_offset = 5  # 阴影偏移量

# 获取文字水印的大小
(text_width, text_height), _ = cv2.getTextSize(text, font, font_scale, 1)

# 创建一个与图片相同大小的透明图像
overlay = image.copy()
overlay = cv2.rectangle(overlay, position, (position[0] + text_width, position[1] + text_height), (0, 0, 0), -1)

# 绘制阴影效果
cv2.putText(overlay, text, (position[0] + shadow_offset, position[1] + shadow_offset), font, font_scale, shadow_color, 1, line_type)

# 将文字水印添加到透明图像上
cv2.putText(overlay, text, position, font, font_scale, font_color, 1, line_type)

# 混合原始图像和透明图像，设置透明度
alpha = 0.8  # 透明度权重
output = cv2.addWeighted(overlay, alpha, image, 1 - alpha, 0)

# 显示添加水印后的图像
cv2.imshow('Watermarked Image', output)
cv2.waitKey(0)
cv2.destroyAllWindows()