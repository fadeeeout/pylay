import json
import time
from datetime import datetime
from random import random

import requests
from util.helper import image_to_base64

prompt_text = """
{
  "3": {
    "inputs": {
      "seed": 456213616421181,
      "steps": 20,
      "cfg": 8,
      "sampler_name": "euler",
      "scheduler": "normal",
      "denoise": 1,
      "model": [
        "22",
        0
      ],
      "positive": [
        "6",
        0
      ],
      "negative": [
        "7",
        0
      ],
      "latent_image": [
        "32",
        0
      ]
    },
    "class_type": "KSampler"
  },
  "4": {
    "inputs": {
      "ckpt_name": "public/chilloutmix_NiPrunedFp32Fix.safetensors"
    },
    "class_type": "CheckpointLoaderSimple"
  },
  "6": {
    "inputs": {
      "text": "1girl,sword",
      "clip": [
        "19",
        0
      ]
    },
    "class_type": "CLIPTextEncode"
  },
  "7": {
    "inputs": {
      "text": "text, watermark,nsfw",
      "clip": [
        "19",
        0
      ]
    },
    "class_type": "CLIPTextEncode"
  },
  "8": {
    "inputs": {
      "samples": [
        "3",
        0
      ],
      "vae": [
        "24",
        0
      ]
    },
    "class_type": "VAEDecode"
  },
  "19": {
    "inputs": {
      "stop_at_clip_layer": -1,
      "clip": [
        "22",
        1
      ]
    },
    "class_type": "CLIPSetLastLayer"
  },
  "22": {
    "inputs": {
      "lora_str": "",
      "model": [
        "4",
        0
      ],
      "clip": [
        "4",
        1
      ]
    },
    "class_type": "MultipleLoraLoader"
  },
  "24": {
    "inputs": {
      "vae_name": "vae-ft-mse-840000-ema-pruned.ckpt",
      "vae": [
        "4",
        2
      ]
    },
    "class_type": "OptionalVAELoader"
  },
  "27": {
    "inputs": {
      "pixels": [
        "37",
        0
      ],
      "vae": [
        "4",
        2
      ]
    },
    "class_type": "VAEEncode"
  },
  "29": {
    "inputs": {
      "upscale_method": "nearest-exact",
      "width": 512,
      "height": 512,
      "crop": "disabled",
      "samples": [
        "27",
        0
      ]
    },
    "class_type": "LatentUpscale"
  },
  "32": {
    "inputs": {
      "amount": 1,
      "samples": [
        "29",
        0
      ]
    },
    "class_type": "RepeatLatentBatch"
  },
  "37": {
    "inputs": {
      "image": ""
    },
    "class_type": "Base64ImageLoader"
  },
  "38": {
    "inputs": {
      "compress": "enable",
      "key_prefix": "user_img",
      "task_id": "caixukun778888",
      "images": [
        "8",
        0
      ]
    },
    "class_type": "Upload2US3"
  }
}
"""

loras = [
    {
        "path": "public/MoXinV1.safetensors",
        "strength": 0.85,
    },
    {
        "path": "public/shukezouma_v1_1.safetensors",
        "strength": 0.75,
    }
]


def inf():
    global prompt_text
    prompt = json.loads(prompt_text)
    prompt['37']['inputs']['image'] = image_to_base64('/Users/user/aigc/temp/guofeng.png')
    prompt["7"]["inputs"]["text"] = ("NSFW,normal quality,low quality,worst "
                                     "quality,cropped,out of frame,jpeg artifacts,blurry,lowres,"
                                     "(watermark),fused fingers,disfigured,too many fingers,bad anatomy")
    prompt["6"]["inputs"]["text"] = ("((Masterpiece)),((best quality)),8k,16k,finely detailed,photorealistic,"
                                     "raw photo,ultra-detailed,shuimobysim,wuchangshuo,"
                                     "bonian,zhenbanqiao,badashanren,shukezouma,1girl,guofeng,sword,")
    prompt["3"]["inputs"]["seed"] = rand_int(1000000000, 9999999999)
    loras_str = json.dumps(loras)
    prompt["22"]["inputs"]["lora_str"] = loras_str
    p = {"prompt": prompt}
    resp = requests.post(url='http://127.0.0.1:8188/prompt', json=p)
    if resp.status_code != 200:
        print(resp.status_code)
        return
    print(resp.json())


def rand_int(min=1, max=255):
    return int(random() * (max - min) + min)


if __name__ == '__main__':
    inf()
