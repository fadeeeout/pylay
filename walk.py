import json
import os


def some_func(prompt, negative_prompt):
    print(f'====== prompt ======')
    print(prompt)
    print(f'====== negative_prompt ======')
    print(negative_prompt)


json_str = '''
{"prompt": "((Masterpiece)),((best quality)),epic composition,ad magazine,RAW photo,8k,16k,hyper-detailed,A running sheep,<lora:environmentart:0.7>",
"negative_prompt": "NSFW, low-res, normal quality, low quality, worst quality, jpeg artifacts, (watermark), ((cropped)), easynegative"
    }'''

if __name__ == '__main__':
    # json_str to dict
    json_dict = json.loads(json_str)
    # dict to variables
    some_func(**json_dict)
