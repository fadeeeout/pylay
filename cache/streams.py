from redis import Redis
from cache.rdb import new_rdb
import traceback


def is_key_exist(r: Redis, key: str) -> bool:
    try:
        return r.exists(key) == 1
    except Exception as e:
        return False


def is_stream_group_exist(r: Redis, stream_key: str, group_name: str) -> bool:
    try:
        if r.exists(stream_key) != 1:
            return False
        groups = r.xinfo_groups(stream_key)
        for group in groups:
            if group['name'] == group_name:
                return True
        return False
    except Exception as e:
        traceback.print_exc()
        return False


def create_streams_group(r: Redis, stream_key: str, group_name: str) -> bool:
    if is_stream_group_exist(r, stream_key, group_name):
        return True
    try:
        r.xgroup_create(stream_key, group_name, mkstream=True)
        return True
    except Exception as e:
        traceback.print_exc()
        return False


class MsgQueue:
    r: Redis = None
    stream_key: str = ''
    group_name: str = ''

    def __init__(self, r: Redis, stream_key: str, group_name: str):
        self.r = r
        self.stream_key = stream_key
        self.group_name = group_name

    def check_group(self):
        if is_stream_group_exist(self.r, self.stream_key, self.group_name):
            return True
        return create_streams_group(self.r, self.stream_key, self.group_name)

    def push(self, msg: dict) -> bool:
        try:
            self.r.xadd(self.stream_key, msg)
            return True
        except Exception as e:
            traceback.print_exc()
            return False


def new_msg_queue(stream_key: str, group_name: str) -> MsgQueue:
    return MsgQueue(new_rdb(), stream_key, group_name)


if __name__ == '__main__':
    print(f'is key exist: {is_stream_group_exist(new_rdb(), "msg_queue", "msg_group")}')
