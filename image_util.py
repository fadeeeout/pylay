import numpy as np
from PIL import Image


def image_info(image_path):
    image = Image.open(image_path)
    arr = np.array(image)
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            # RGBA 分别对应 arr[i, j, 0], arr[i, j, 1], arr[i, j, 2], arr[i, j, 3]
            # 如果alpha通道为0，说明是透明区域
            if 0 < arr[i, j, 3] < 255:
                # 依次是R,G,B
                print(f'{arr[i, j, 0]} {arr[i, j, 1]} {arr[i, j, 2]} {arr[i, j, 3]}')


if __name__ == '__main__':
    image_path = "/Users/user/aigc/temp/test_mask_black.png"
    image_info(image_path)
