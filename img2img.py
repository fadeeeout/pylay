import base64
import json
from io import BytesIO
import requests
from PIL import Image


def pil_to_base64(img) -> str:
    buffered = BytesIO()
    img.save(buffered, format="PNG")
    img_str = base64.b64encode(buffered.getvalue())
    return img_str.decode('utf-8')


def open_image(path):
    return Image.open(path)


def image_p(path):
    return pil_to_base64(open_image(path))


init_image = "/Users/user/aigc/pic/test_ori.png"
mask = "/Users/user/aigc/pic/test_mask.png"


def img_2_img_payload():
    # return {
    #     "prompt": "((Masterpiece)),((best quality)),epic composition,ad magazine,RAW photo,8k,16k,hyper-detailed,A running "
    #               "sheep,<lora:environmentart:0.7>",
    #     "negative_prompt": "NSFW, low-res, normal quality, low quality, worst quality, jpeg artifacts, (watermark), "
    #                        "((cropped)), easynegative",
    #     "init_images": [image_p(init_image)],
    #     "mask": image_p(mask),
    #     "steps": 20,
    #     "width": 768
    # }
    json_file = "/Users/user/work/repos/aigc/srv-aigc-draw-instance/test.json"
    with open(json_file, 'r') as f:
        json_str = f.read()
        return json.loads(json_str)


def base64_to_pil(base64_str):
    im_b64 = bytes(base64_str, 'utf-8')
    im_bytes = base64.b64decode(im_b64)  # im_bytes is a binary image
    im_file = BytesIO(im_bytes)  # convert image to file-like object
    img = Image.open(im_file)
    return img


def save_base64_image(base64_str, path):
    img = base64_to_pil(base64_str)
    img.save(path, "PNG")


def send_request(base_url):
    import requests
    data = img_2_img_payload()
    data['denoising_strength'] = 0.7
    response = requests.post(base_url + "/sdapi/v1/img2img", json=data)
    print(response.status_code)
    result = response.json()
    idx = 1
    for image in enumerate(result["images"]):
        # print(image[1])
        save_base64_image(image[1], f"/Users/user/aigc/pic/test_result_{idx}.png")
        idx += 1


def pil_2_base64(img: Image):
    buffered = BytesIO()
    img.save(buffered, format="PNG")
    img_str = base64.b64encode(buffered.getvalue())
    return img_str.decode('utf-8')


def test_compress(image_dir: str, image_file: str):
    img = Image.open(f'{image_dir}/{image_file}')
    payload = {
        "image": pil_2_base64(img),
        "max_quality": 100,
        "min_quality": 50,
    }
    url = "http://127.0.0.1:6523/compress"
    response = requests.post(url, json=payload)
    print(response.status_code)
    result = response.json()
    image = result['image']
    if image is None or image == "":
        print("压缩失败")
        return
    img = base64_to_pil(image)
    img.save(f'{image_dir}/compressed_{image_file}', "PNG")


if __name__ == '__main__':
    # base_url = "http://117.50.193.192:7861"
    # send_request(base_url)
    image_dir = "/Users/user/aigc/temp/test/"
    image_file = "exhorse.png"
    test_compress(image_dir, image_file)
