import matplotlib
import pandas as pd
from PIL import Image
from pandas import DataFrame
from dataclasses import dataclass
from typing import Tuple, List, Union
import os
from pandas.plotting._matplotlib.style import get_standard_colors
from matplotlib import axes


@dataclass()
class ParsedValues:
    loss: DataFrame
    lr: DataFrame
    ram: DataFrame
    merged: bool


def convert_tfevent(filepath) -> Tuple[DataFrame, DataFrame, DataFrame, bool]:
    loss_events = []
    lr_events = []
    ram_events = []
    instance_loss_events = []
    prior_loss_events = []
    has_all = False
    try:
        from tensorboard.backend.event_processing.event_accumulator import EventAccumulator
    except:
        print("Unable to import tensorboard")
        return pd.DataFrame(loss_events), pd.DataFrame(lr_events), pd.DataFrame(ram_events), has_all

    acc = EventAccumulator(filepath).Reload()

    def parse_tbevent(tbevent, name):
        return {
            "Wall_time": tbevent.wall_time,
            "Name": name,
            "Step": tbevent.step,
            "Value": float(tbevent.value),
        }

    for k, v in acc.Tags().items():
        if k == "scalars":  # Assume only scalars are logged
            # e.g. ['lr', 'loss', 'inst_loss', 'prior_loss', 'vram', 'epoch_loss']
            for scalar in v:
                for tbevent in acc.Scalars(scalar):  # multiple steps
                    if scalar == "lr":
                        lr_events.append(parse_tbevent(tbevent, scalar))
                    elif scalar == "loss":
                        loss_events.append(parse_tbevent(tbevent, scalar))
                    elif scalar == "vram_usage" or scalar == "vram":
                        ram_events.append(parse_tbevent(tbevent, scalar))
                    elif scalar == "instance_loss" or scalar == "inst_loss":
                        instance_loss_events.append(
                            parse_tbevent(tbevent, scalar))
                    elif scalar == "prior_loss":
                        prior_loss_events.append(
                            parse_tbevent(tbevent, scalar))

    # Merge "lr, loss, instance_loss, prior_loss" into the merged_events
    # if the steps are all the same, make loss_events = merged_events
    merged_events = []
    has_all = True
    for le in loss_events:
        lr = next(
            (item for item in lr_events if item["Step"] == le["Step"]), None)
        instance_loss = next(
            (item for item in instance_loss_events if item["Step"] == le["Step"]), None)
        prior_loss = next(
            (item for item in prior_loss_events if item["Step"] == le["Step"]), None)
        if lr is not None and instance_loss is not None and prior_loss is not None:
            le["LR"] = lr["Value"]
            le["Loss"] = le["Value"]
            le["Instance_Loss"] = instance_loss["Value"]
            le["Prior_Loss"] = prior_loss["Value"]
            merged_events.append(le)
        else:
            has_all = False
    if has_all:
        loss_events = merged_events

    return pd.DataFrame(loss_events), pd.DataFrame(lr_events), pd.DataFrame(ram_events), has_all

@dataclass
class YAxis:
    name: str
    columns: List[str]


@dataclass
class PlotDefinition:
    title: str
    x_axis: str
    y_axis: List[YAxis]


class LogParser:
    def __init__(self, logging_dir):
        self.logging_dir = logging_dir
        self.model_name = None
        self.parsed = {}
        self.out_loss = []
        self.out_lr = []
        self.out_ram = []
        self.parsed_files = {}
        self.smoothing_window = 50

    def plot_multi_alt(
            self,
            data: pd.DataFrame,
            plot_definition: PlotDefinition,
            spacing: float = 0.1,
    ):
        styles = ["-", ":", "--", "-."]
        colors = get_standard_colors(num_colors=7)
        loss_color = colors[0]
        avg_colors = colors[1:]
        for i, yi in enumerate(plot_definition.y_axis):
            if len(yi.columns) > len(styles):
                raise ValueError(
                    f"Maximum {len(styles)} traces per yaxis allowed. If we want to allow this we need to add some logic.")
            if i > len(colors):
                raise ValueError(
                    f"Maximum {len(colors)} yaxis axis allowed. If we want to allow this we need to add some logic.")

            if i == 0:
                ax = data.plot(
                    x=plot_definition.x_axis,
                    y=yi.columns,
                    title=plot_definition.title,
                    color=[loss_color] * len(yi.columns)
                )
                ax.set_ylabel(ylabel=yi.name)

            else:
                # Multiple y-axes
                ax_new = ax.twinx()
                ax_new.spines["right"].set_position(("axes", 1 + spacing * (i - 1)))
                data.plot(
                    ax=ax_new,
                    x=plot_definition.x_axis,
                    y=yi.columns,
                    color=[avg_colors[yl] for yl in range(len(yi.columns))]
                )
                ax_new.set_ylabel(ylabel=yi.name)

        ax.legend(loc=0)

        return ax

    def log_parse(self):
        columns_order = ['Wall_time', 'Name', 'Step', 'Value']
        for (root, _, filenames) in os.walk(self.logging_dir):
            for filename in filenames:
                if "events.out.tfevents" not in filename and "dreambooth.events" not in filename:
                    continue
                file_full_path = os.path.join(root, filename)
                f_time = os.path.getmtime(file_full_path)
                do_parse = True
                if file_full_path in self.parsed_files.keys():
                    e_time = self.parsed_files[file_full_path]
                    if e_time != f_time:
                        print(f"Log file updated, re-parsing: {file_full_path}")
                    else:
                        print(f"Log file unchanged, nothing to do: {file_full_path}")
                        do_parse = False
                if do_parse:
                    self.parsed_files[file_full_path] = f_time
                    converted_loss, converted_lr, converted_ram, merged = convert_tfevent(file_full_path)
                    self.parsed[file_full_path] = ParsedValues(converted_loss, converted_lr, converted_ram, merged)

        out_loss = []
        out_lr = []
        out_ram = []
        has_all_lr = True

        for file, data in self.parsed.items():
            out_loss.append(data.loss)
            out_lr.append(data.lr)
            out_ram.append(data.ram)
            if not data.merged:
                has_all_lr = False

        loss_columns = columns_order
        if has_all_lr:
            loss_columns = ['Wall_time', 'Name', 'Step', 'Loss', "LR", "Instance_Loss", "Prior_Loss"]
        # Concatenate (and sort) all partial individual dataframes
        # print(f'[DEBUG] out_loss: [[{out_loss}]] loss_columns: [[{loss_columns}]] has_all_lr: [[{has_all_lr}]]')
        all_df_loss = pd.concat(out_loss)
        print(list(all_df_loss.columns))
        all_df_loss = all_df_loss[loss_columns]
        all_df_loss = all_df_loss.fillna(method="ffill")
        all_df_loss = all_df_loss.sort_values("Wall_time")
        all_df_loss = all_df_loss.reset_index(drop=True)
        sw = int(self.smoothing_window if self.smoothing_window < len(all_df_loss) / 3 else len(all_df_loss) / 3)
        all_df_loss = all_df_loss.rolling(sw).mean(numeric_only=True)

        out_images = []
        out_names = []

        if has_all_lr:
            plotted_loss = self.plot_multi_alt(
                all_df_loss,
                plot_definition=PlotDefinition(
                    title=f"Loss Average/Learning Rate",
                    x_axis="Step",
                    y_axis=[
                        YAxis(name="LR", columns=["LR"]),
                        YAxis(name="Loss", columns=["Instance_Loss", "Prior_Loss", "Loss"]),

                    ]
                )
            )
            loss_name = "Loss Average/Learning Rate"
        else:
            plotted_loss = all_df_loss.plot(x="Step", y="Value", title="Loss Averages")
            loss_name = "Loss Averages"
            all_df_lr = pd.concat(out_lr)[columns_order]
            all_df_lr = all_df_lr.sort_values("Wall_time")
            all_df_lr = all_df_lr.reset_index(drop=True)
            all_df_lr = all_df_lr.rolling(self.smoothing_window).mean(numeric_only=True)
            plotted_lr = all_df_lr.plot(x="Step", y="Value", title="Learning Rate")
            img_name = f'lr_plot_111.png'
            # lr_img = os.path.join(model_config.model_dir, "logging", f"lr_plot_{model_config.revision}.png")
            lr_img = os.path.join(self.logging_dir, "logging", img_name)
            plotted_lr.figure.savefig(lr_img)
            matplotlib.pyplot.close(plotted_lr.figure)
            log_lr = Image.open(lr_img)
            out_images.append(log_lr)
            out_names.append("Learning Rate")

        img_name = f'loss_plot_111.png'
        # loss_img = os.path.join(model_config.model_dir, "logging", f"loss_plot_{model_config.revision}.png")
        loss_img = os.path.join(self.logging_dir, img_name)
        print(f"Saving {loss_img}")
        plotted_loss.figure.savefig(loss_img)
        matplotlib.pyplot.close(plotted_loss.figure)
        log_pil = Image.open(loss_img)
        out_images.append(log_pil)
        out_names.append(loss_name)


if __name__ == '__main__':
    lp = LogParser("/Users/user/work/temp/log/")
    lp.log_parse()
