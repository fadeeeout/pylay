import json

json_str = '''{
    "model_list": [
        "control_v11e_sd15_ip2p [c4bb465c]",
        "control_v11e_sd15_shuffle [526bfdae]",
        "control_v11f1e_sd15_tile [a371b31b]",
        "control_v11f1p_sd15_depth [cfd03158]",
        "control_v11p_sd15_canny [d14c016b]",
        "control_v11p_sd15_inpaint [ebff9138]",
        "control_v11p_sd15_lineart [43d4be0d]",
        "control_v11p_sd15_mlsd [aca30ff0]",
        "control_v11p_sd15_normalbae [316696f1]",
        "control_v11p_sd15_openpose [cab727d4]",
        "control_v11p_sd15_scribble [d4ba51ff]",
        "control_v11p_sd15_seg [e1f51eb9]",
        "control_v11p_sd15_softedge [a8575a2a]",
        "control_v11p_sd15s2_lineart_anime [3825e83e]"
    ]
}'''


def to_dict(string):
    return json.loads(string)


def get_model_list(json_dict):
    return json_dict['model_list']


def to_csv(model_list):
    # 数据格式：model_name [hash]
    # 提取出 model_name 和 hash
    # 表头：模型名,hash,大小,us3bucket,us3bucket(测试环境),us3路径
    # 只有模型名和hash来自json，其他都是都是相同的
    # 比如：control_v11e_sd15_ip2p,c4bb465c,1.49GB=1525.76MB,aigc-picpik-model,aigc-test-picpik-model,ControlNet/
    content = ''
    for model in model_list:
        hash_str = model.split('[')[1].split(']')[0]
        model_name = model.split('[')[0].strip()
        save_str = f'{model_name},{hash_str},\\\"{model}\\\",1.49GB=1525.76MB,aigc-picpik-model,aigc-test-picpik-model,ControlNet/\n'
        content += save_str
    with open('models.csv', 'w') as f:
        f.write(content)


if __name__ == '__main__':
    json_dict = to_dict(json_str)
    model_list = get_model_list(json_dict)
    to_csv(model_list)
