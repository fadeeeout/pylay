import base64
from io import BytesIO
import requests
from PIL import Image


def base64_to_pil(im_b64) -> Image:
    im_b64 = bytes(im_b64, 'utf-8')
    im_bytes = base64.b64decode(im_b64)  # im_bytes is a binary image
    im_file = BytesIO(im_bytes)  # convert image to file-like object
    img = Image.open(im_file)
    img = img.convert("RGBA")
    return img


def pil_to_base64(img) -> str:
    buffered = BytesIO()
    img.save(buffered, format="PNG")
    img_str = base64.b64encode(buffered.getvalue())
    return img_str.decode('utf-8')


def face_restore(image_path):
    i = Image.open(image_path)
    base64_img = pil_to_base64(i)
    payload = {
        "image": base64_img,
        "model": "CodeFormer"
    }
    rsp = requests.post("http://106.75.85.50:7861/service/v1/face-restore", json=payload)
    # get image in base64 format from rsp.json()
    result = rsp.json()
    img = result['restored_image']
    if img is None:
        print(f'人脸修复失败：{result["message"]}')
    if img == "":
        print(f'人脸修复失败：{result["message"]}')
    i = base64_to_pil(img)
    i.save("restored.png")


if __name__ == '__main__':
    image_dir = "/Users/user/aigc/pic/bad_face.png"
    face_restore(image_dir)
