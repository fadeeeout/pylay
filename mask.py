import numpy as np
from PIL import Image

from util.helper import base64_to_pil


def save_image(image, path):
    image.save(path, 'PNG')


def image_revert_1(image_path):
    image = Image.open(image_path)
    arr = np.array(image)
    new_data = np.empty(arr.shape, dtype=np.uint8)
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            alpha = arr[i, j, 3]
            if alpha == 0:
                new_data[i, j] = [0, 0, 0, 0]
            elif 0 < alpha < 255:
                new_data[i, j] = [0, 0, 0, alpha]
            else:
                new_data[i, j] = [255, 255, 255, alpha]
    new_img = Image.fromarray(new_data, mode="RGBA")
    return new_img

def image_revert(image_path):
    image = Image.open(image_path)
    arr = np.array(image)
    new_data = np.empty(arr.shape, dtype=np.uint8)
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            alpha = arr[i, j, 3]
            if alpha == 0:
                new_data[i, j] = [255, 255, 255, 0]
            elif 0 < alpha < 255:
                new_data[i, j] = [255, 255, 255, alpha]
            else:
                new_data[i, j] = [0, 0, 0, alpha]
    new_img = Image.fromarray(new_data, mode="RGBA")
    return new_img

def HWC3(x):
    assert x.dtype == np.uint8
    if x.ndim == 2:
        x = x[:, :, None]
    assert x.ndim == 3
    H, W, C = x.shape
    assert C == 1 or C == 3 or C == 4
    if C == 3:
        return x
    if C == 1:
        return np.concatenate([x, x, x], axis=2)
    if C == 4:
        color = x[:, :, 0:3].astype(np.float32)
        alpha = x[:, :, 3:4].astype(np.float32) / 255.0
        y = color * alpha + 255.0 * (1.0 - alpha)
        y = y.clip(0, 255).astype(np.uint8)
        return y


if __name__ == '__main__':
    # image_path = "/Users/user/aigc/pic/left_top.png"
    # new_img_path = "/Users/user/aigc/pic/left_top_new.png"
    # image = image_revert(image_path)
    # save_image(image, new_img_path)

    # img_path = "/Users/user/aigc/temp/test/origin.png"
    # img = Image.open(img_path).convert("RGBA")
    # arr = np.array(img)
    mask_path = "/Users/user/aigc/temp/dogge.png"
    new_img = image_revert_1(mask_path)
    new_img.save("/Users/user/aigc/temp/dogge_new_mask.png", 'PNG')
