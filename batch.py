import os

from PIL import Image


def nopng_to_png(image_path, filename):
    file_path = f'{image_path}/{filename}'
    if os.path.exists(file_path):
        ext = os.path.splitext(filename)[1]
        old_filename = os.path.splitext(filename)[0]
        if ext == '.png' or ext not in ['.jpg', '.jpeg', '.webp', '.avif']:
            return
        img = Image.open(file_path)
        img = img.convert("RGBA")
        img.save(f'{image_path}/{old_filename}.png', "PNG")
        os.remove(file_path)
    else:
        print(f'file not exists: {file_path}')


def batch_nopng_to_png(image_path):
    for filename in os.listdir(image_path):
        nopng_to_png(image_path, filename)


if __name__ == '__main__':
    image_dir = "/Users/user/aigc/pic/jason_statham"
    batch_nopng_to_png(image_dir)
