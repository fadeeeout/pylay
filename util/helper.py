import base64
import os
from io import BytesIO
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image


def image_to_base64(image_path):
    if not os.path.exists(image_path):
        print(f'图片不存在：{image_path}')
        return ""
    i = Image.open(image_path).convert("RGB")
    buffered = BytesIO()
    i.save(buffered, format="PNG")
    img_str = base64.b64encode(buffered.getvalue())
    return img_str.decode('utf-8')


def base64_to_pil(data: str) -> Image:
    if data is None or data == "":
        return None
    im_b64 = bytes(data, 'utf-8')
    im_bytes = base64.b64decode(im_b64)  # im_bytes is a binary image
    im_file = BytesIO(im_bytes)  # convert image to file-like object
    img = Image.open(im_file)
    return img


def base64_to_np(data: str) -> np.ndarray:
    if data is None or data == "":
        return None
    im_b64 = bytes(data, 'utf-8')
    im_bytes = base64.b64decode(im_b64)  # im_bytes is a binary image
    im_file = BytesIO(im_bytes)  # convert image to file-like object
    img = Image.open(im_file)
    return np.array(img)


def save_b64_img(data: str, path: str) -> None:
    if data is None or data == "":
        print("数据为空")
        return
    if not os.path.exists(os.path.dirname(path)):
        print("目录不存在")
        return
    img = base64_to_pil(data)
    if img is None:
        print("转换失败")
        return
    img.save(path, "PNG")


def pil_to_base64(img) -> str:
    buffered = BytesIO()
    img.save(buffered, format="PNG")
    img_str = base64.b64encode(buffered.getvalue())
    return img_str.decode('utf-8')


def show_b64_img(data: str) -> None:
    if data is None or data == "":
        print("数据为空")
        return
    img = base64_to_pil(data)
    if img is None:
        print("转换失败")
        return
    plt.imshow(img)
    plt.show()
