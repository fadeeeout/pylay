import re

strings = [
    "dreambooth123",
    "hello",
    "dreambooth456",
    "world",
    "dreambooth/cicqsm5rh4mpndfrg2c0_1690272341_civo4ldrh4mjqan4nkig/db_config.json"
]

pattern = r"^(?!dreambooth).*$"

for string in strings:
    if re.match(pattern, string):
        print(f"Matched: {string}")
    else:
        print(f"Not matched: {string}")