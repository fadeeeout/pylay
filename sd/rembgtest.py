import os
from datetime import datetime
from random import random
import matplotlib.pyplot as plt
from PIL import Image
import requests

from util.helper import image_to_base64, save_b64_img

models = ['silueta', 'isnet-anime']
image_dir = '/Users/user/aigc/temp/test/rembg/temp'

images_list = list()


def init_images_list():
    # 遍历图片文件夹，获取所有png图片
    for root, dirs, files in os.walk(image_dir):
        for file in files:
            if file.endswith(".png"):
                images_list.append(os.path.join(root, file))

    print(f'图片数量：{len(images_list)}')


def rand_int(min=1, max=255):
    return int(random() * (max - min) + min)


def rembg_payload(image_path, model, foreground, background, box=None, auto=True):
    if box is None:
        box = []
    img = image_to_base64(image_path)
    return {
        "input_image": img,
        "model": model,
        "box": box,
        "return_mask": False,
        "alpha_matting": True,
        "alpha_matting_foreground_threshold": foreground,
        "alpha_matting_background_threshold": background,
        "alpha_matting_erode_size": 10
    }


def batch_rembg_payload(paths,return_mask=False, auto=True):
    payload = {
        "input_images": [image_to_base64(path) for path in paths],
        "return_mask": return_mask,
        "auto": auto
    }
    return payload


def advanced_rembg_payload(image_path,positive_points,negative_points, foreground, background, box=None, auto=True, return_mask=False):
    if box is None:
        box = []
    img = image_to_base64(image_path)
    return {
        "input_image": img,
        "positive_points": positive_points,
        "negative_points": negative_points,
        "return_mask": return_mask,
        "alpha_matting": True,
        "alpha_matting_foreground_threshold": foreground,
        "alpha_matting_background_threshold": background,
        "alpha_matting_erode_size": 10,
        "auto": auto
    }


def do_rembg(payload: dict, uri='/rembg'):
    url = f"http://127.0.0.1:7861{uri}"
    req_start = datetime.now()
    resp = requests.post(url=url, json=payload)
    if resp.status_code != 200:
        print(f'状态码：{resp.status_code}')
        print(f'错误信息：{resp.json()}')
        return None, None
    response = resp.json()
    cost = datetime.now() - req_start
    if response['code'] and response['code'] != 200:
        print(f'错误信息：{response["message"]}')
        return None, cost
    return response, cost


default_foreground = 240
default_background = 10


def get_full_filename(file_path):
    pairs = file_path.rsplit('/', 1)
    return pairs[len(pairs) - 1]


def del_temp_img():
    temp_path = "/Users/user/aigc/temp/test/rembg/temp/temp.png"
    os.remove(temp_path)


def to_seconds(delta):
    string = f'{delta}'
    hours = float(string.split(':')[0])
    minutes = float(string.split(':')[1])
    seconds = float(string.split(':')[2])
    return hours * 3600 + minutes * 60 + seconds


class RemResult:
    payload: dict = {}
    result_image: str = ''
    original_img_path: str = ''
    batch_no: int = 1
    task_cost = None

    def __init__(self, payload_, result_img, origin, batch_no=1, task_cost=None):
        self.payload = payload_
        self.result_image = result_img
        self.original_img_path = origin
        self.batch_no = batch_no
        self.task_cost = task_cost

    def elapsed(self):
        return to_seconds(self.task_cost)

    def save_temp_img(self):
        temp_path = "/Users/user/aigc/temp/test/rembg/temp/temp.png"
        save_b64_img(self.result_image, temp_path)

    def full_filename(self):
        pairs = self.original_img_path.rsplit('/', 1)
        return pairs[len(pairs) - 1]

    def filename(self):
        full = self.full_filename()
        return full.rsplit('.', 1)[0]

    def save_filename(self):
        save_dir = f'/Users/user/aigc/temp/test/rembg/{self.filename()}'
        if not os.path.exists(save_dir):
            os.mkdir(save_dir)
        full_path = f'{save_dir}/{self.filename()}_{self.payload["model"]}_{self.batch_no}.png'
        return full_path

    def plot_and_save(self):
        self.save_temp_img()
        plt.figure(figsize=(10, 10))
        ax1 = plt.subplot(1, 2, 1)
        ax1.axis('off')
        ax1.title.set_text(f'{self.full_filename()}  model: {self.payload["model"]}')
        ax1.imshow(plt.imread(self.original_img_path, 'png'))
        ax2 = plt.subplot(1, 2, 2)
        ax2.axis('off')
        ax2.title.set_text(f'foreground: {self.payload["alpha_matting_foreground_threshold"]}, '
                           f'background: {self.payload["alpha_matting_background_threshold"]}')
        ax2.imshow(plt.imread("/Users/user/aigc/temp/test/rembg/temp/temp.png", 'png'))
        plt.savefig(self.save_filename())
        plt.close()
        del_temp_img()

    def string(self):
        return f'{self.full_filename()}, {self.payload["model"]}, {self.payload["alpha_matting_foreground_threshold"]},' + \
            f'{self.payload["alpha_matting_background_threshold"]},' + \
            f'{self.payload["alpha_matting_erode_size"]}, {self.elapsed()},{self.batch_no}'
    


def single_test():
    image = "/Users/user/aigc/temp/test/rembg/temp/girl_ani.png"
    payload = rembg_payload(image, 'silueta', 210, 170,auto=False)
    rsp, cost = do_rembg(payload)
    if rsp is None:
        print(f'耗时：{to_seconds(cost)}秒')
        exit(1)
    print(f'耗时：{to_seconds(cost)}秒')
    save_b64_img(rsp['image'], "/Users/user/aigc/temp/test/rembg/temp/result/single-1.png")


def advanced_test():
    image = "/Users/user/aigc/temp/test/rembg/temp/plants-1.jpg"
    positive_point = [[724,740]]
    negative_point = []
    payload = advanced_rembg_payload(image, positive_point, negative_point, 240, 10,auto=True, return_mask=False)
    rsp, cost = do_rembg(payload, '/rembg/advanced')
    if rsp is None:
        print(f'耗时：{to_seconds(cost)}秒')
        exit(1)
    print(f'耗时：{to_seconds(cost)}秒')
    save_b64_img(rsp['image'], "/Users/user/aigc/temp/test/rembg/temp/advanced-3.png")



def batch_test():
    image_path_base = "/Users/user/aigc/temp/test/rembg/temp"
    result_base = "/Users/user/aigc/temp/test/rembg/temp/result"
    images = [
        f'{image_path_base}/girl.png',
        f'{image_path_base}/girl_ani.png',
        f'{image_path_base}/boys.webp',
        f'{image_path_base}/portrait.jpeg',
    ]
    payload = batch_rembg_payload(images, return_mask=True, auto=True)
    rsp, cost = do_rembg(payload, '/rembg/batch')
    if rsp is None:
        print(f'耗时：{to_seconds(cost)}秒')
        exit(1)
    print(f'耗时：{to_seconds(cost)}秒')
    image_list = rsp['images']
    for i in range(len(image_list)):
        save_b64_img(image_list[i], f'{result_base}/re_mask_{i}.png')


# if __name__ == '__main__':
#     # image_p = "/Users/user/aigc/temp/girl1.png"
#     # cropped = Image.open(image_p).crop((180, 264, 510, 642))
#     # p = rembg_payload(image_p, 'silueta', 140, 110, [180, 264, 510, 642])
#     # rsp, cost = do_rembg(p)
#     # if rsp is None:
#     #     print(f'耗时：{to_seconds(cost)}秒')
#     #     exit(1)
#     # print(f'耗时：{to_seconds(cost)}秒')
#     # save_b64_img(rsp['image'], "/Users/user/aigc/temp/save_test.png")
#     pass
