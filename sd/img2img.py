import requests
from util.helper import save_b64_img, image_to_base64


def img_to_img_payload(image_path: str) -> dict:
    img = image_to_base64(image_path)
    origin = image_to_base64("/Users/user/aigc/temp/test/es1.png")
    mask = image_to_base64("/Users/user/aigc/temp/test/es2.png")
    return {
        "alwayson_scripts": {
            "controlnet": {
                "args": [
                    {
                        "input_image": origin,
                        "module": "depth",
                        "model": "control_v11f1p_sd15_depth [cfd03158]",
                        "weight": 1,
                        "lowvram": True,
                        "processor_res": 512,
                        "guidance_end": 1
                    },
                    {
                        "input_image": origin,
                        "module": "ip-adapter_clip_sd15",
                        "model": "ip-adapter_sd15_light [be1c9b97]",
                        "weight": 1,
                        "lowvram": True,
                        "processor_res": 512,
                        "guidance_end": 1,
                        "enable": True,
                    },
                    {
                        "input_image": origin,
                        "module": "lineart_standard",
                        "model": "control_v11p_sd15_lineart [43d4be0d]",
                        "weight": 1,
                        "lowvram": True,
                        "processor_res": 512,
                        "guidance_end": 1
                    }
                ]
            }
        },
        "batch_size": 1,
        "cfg_scale": 7.5,
        "denoising_strength": 0.69,
        "height": 400,
        "init_images": [mask],
        "inpaint_full_res": False,
        "inpaint_full_res_padding": 0,
        "inpainting_fill": 0,
        "inpainting_mask_invert": 0,
        "mask_blur": 0,
        "negative_prompt": "NSFW,low-res,normal quality,low quality,worst quality,jpeg artifacts,(watermark),((cropped)),(blurry),drawing,painting,sketches,glowing,deformed,easynegative",
        "prompt": "((Masterpiece)),((best quality)),8k,16k,finely detailed,ue 5,octane renderer,photo realistic,purism,a computer rendering,3D product render",
        "resize_mode": 2,
        "sampler_name": "Euler",
        "seed": -1,
        "steps": 20,
        "width": 400
    }


def img_to_img(payload: dict, base_url: str):
    url = f'{base_url}/sdapi/v1/img2img'
    resp = requests.post(url=url, json=payload)
    print(resp.status_code)
    return resp.json()


if __name__ == '__main__':
    image_path = "/Users/user/aigc/temp/test/es3.png"
    payload = img_to_img_payload(image_path)
    base_url = "http://127.0.0.1:7861"
    result = img_to_img(payload, base_url)
    idx = 1
    for image in enumerate(result["images"]):
        save_b64_img(image[1], f"/Users/user/aigc/temp/test/test_result_{idx}.png")
        idx += 1
