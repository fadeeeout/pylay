import requests
from util.helper import save_b64_img, image_to_base64


def txt_to_img_payload() -> dict:
    # origin = image_to_base64("/Users/user/aigc/temp/test/origin.png")
    # mask = image_to_base64("/Users/user/aigc/temp/test/mask_new.png")
    origin = image_to_base64("/Users/user/aigc/temp/test/origin_no.png")
    mask = image_to_base64("/Users/user/aigc/temp/test/mask_no_new.png")
    return {
        "alwayson_scripts": {
            "controlnet": {
                "args": [
                    {
                        "input_image": [origin, mask],
                        "module": "inpaint_only+lama",
                        "model": "control_v11p_sd15_inpaint [ebff9138]",
                        "weight": 1,
                        "resize_mode": 2,
                        "lowvram": True,
                        "processor_res": 1230,
                        "guidance_end": 1,
                        "control_mode": 2,
                        "pixel_perfect": True,
                    }
                ]
            }
        },
        "cfg_scale": 8,
        "height": 750,
        "negative_prompt": "NSFW,normal quality,low quality,worst quality,cropped,out of frame,jpeg artifacts,blurry,"
                           "lowres,(watermark),fused fingers,disfigured,too many fingers,bad anatomy,easynegative",
        "prompt": "best quality,photorealistic,raw photo,ultra-detailed",
        "sampler_name": "Euler a",
        "width": 1230,
        "steps": 20
    }


def txt_to_img(payload: dict, base_url: str):
    url = f'{base_url}/sdapi/v1/txt2img'
    resp = requests.post(url=url, json=payload)
    print(resp.status_code)
    return resp.json()


def test_adetailer():
    payload = txt_to_img_payload()
    base_url = "http://127.0.0.1:7861"
    result = txt_to_img(payload, base_url)
    idx = 1
    for image in enumerate(result["images"]):
        save_b64_img(image[1], f"/Users/user/aigc/temp/test/test_result_t2i2222_{idx}.png")
        idx += 1


if __name__ == '__main__':
    test_adetailer()
