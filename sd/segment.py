import os.path
import time

import requests
import segmentation_refinement as refine
from PIL import Image
from util.helper import show_b64_img, image_to_base64, base64_to_np, save_b64_img, pil_to_base64
from xid import XID
import cv2


def sam_predict_payload(image_path: str) -> dict:
    img = image_to_base64(image_path)
    return {
        "sam_model_name": "sam_vit_h_4b8939.pth",
        "input_image": img,
        "sam_positive_points": [],
        "sam_negative_points": [],
        "dino_enabled": True,
        # "dino_model_name": "GroundingDINO_SwinT_OGC (694MB)",
        "dino_model_name": "GroundingDINO_SwinB (938MB)",
        "dino_text_prompt": "glasses",
        "dino_box_threshold": 0.3,
        "dino_preview_checkbox": True,
        "dino_preview_boxes_selection": [
            0
        ]
    }


def sam_predict(payload: dict, base_url: str):
    url = f'{base_url}/sam/sam-predict'
    resp = requests.post(url=url, json=payload)
    if resp.status_code != 200:
        print(f'状态码：{resp.status_code}')
        print(f'错误信息：{resp.json()}')
        exit(1)
    return resp.json()


def refine_seg(origin_img: str, mask: str):
    t_img = TempImg(origin_img)
    img_c = t_img.cv2_read()
    t_mask = TempImg(mask)
    mask_c = t_mask.cv2_read(flags=cv2.IMREAD_GRAYSCALE)
    refiner = refine.Refiner(device='cpu')
    output = refiner.refine(img_c, mask_c)
    t_mask.clean_up()
    t_img.clean_up()
    return output


def choose_best(masks: list) -> str:
    if masks is None or len(masks) == 0:
        return ""
    if len(masks) >= 3:
        return masks[2]
    if len(masks) == 2:
        return masks[1]
    return masks[0]


class TempImg:
    _path = "/Users/user/aigc/.temp"
    _img_path = ""

    def __init__(self, img: str):
        self.img = img
        self.save_temp()

    def save_temp(self):
        if not os.path.exists(path=self._path):
            os.mkdir(self._path)
        timestamp = int(time.time())
        self._img_path = os.path.join(self._path, f"{timestamp}_{XID()}.png")
        save_b64_img(self.img, self._img_path)

    def clean_up(self):
        if os.path.exists(self._img_path):
            os.remove(self._img_path)

    def cv2_read(self, flags=cv2.IMREAD_COLOR):
        return cv2.imread(self._img_path, flags=flags)


def crop_img(original: Image, masked: Image):
    cropped = Image.new(original.mode, masked.size)
    cropped.paste(original, (0, 0), masked)
    return cropped


def refine_img():
    img = cv2.imread("/Users/user/aigc/temp/portrait.jpeg", cv2.IMREAD_COLOR)
    mask = cv2.imread("/Users/user/aigc/temp/girl_mask.png", cv2.IMREAD_GRAYSCALE)
    refiner = refine.Refiner(device='cpu')
    output = refiner.refine(img, mask)
    mask_refined = Image.fromarray(output)
    cropped = crop_img(Image.open("/Users/user/aigc/temp/portrait.jpeg"), mask_refined)
    cropped.save("/Users/user/aigc/temp/cropped.png", "PNG")


if __name__ == '__main__':
    refine_img()
    # image_file = "/Users/user/aigc/temp/portrait.jpeg"
    # payload = sam_predict_payload(image_file)
    # base_url = "http://127.0.0.1:7861"
    # result = sam_predict(payload, base_url)
    #
    # if result['msg'] is not None and result['msg'] != "":
    #     print(result['msg'])
    #
    # masks = result['masks']
    # masked_images = result['masked_images']
    # mask = choose_best(masks)
    # save_b64_img(mask, "/Users/user/aigc/pic/glasses_mask.png")
    # # output = refine_seg(image_to_base64(image_file), mask)
    # show_b64_img(mask)
    # # result = Image.fromarray(output)
    # # show_b64_img(pil_to_base64(result))
