import numpy as np
from PIL import Image


def is_only_white_and_black(mask: Image) -> bool:
    arr = np.array(mask)
    return


def white_to_transparent(mask: Image) -> Image:
    # 将白色像素全部转成透明
    arr = np.array(mask)
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            r, g, b, a = arr[i, j, :]
            if r == 255 and g == 255 and b == 255:
                arr[i, j, :] = [255, 255, 255, 0]
    return Image.fromarray(arr)


def is_black_and_white(image):
    arr = np.array(image)
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            r, g, b = arr[i, j, :3]
            if r != g or g != b:
                return False
    return True

origin_image_path = "/Users/user/aigc/temp/portrait.jpeg"
origin = Image.open(origin_image_path).convert('RGBA')
image_path = "/Users/user/aigc/temp/newmask.png"

img = Image.open(image_path).convert('RGBA')

new_img = white_to_transparent(img)
# origin.crop(new_img.getbbox()).save("/Users/user/aigc/temp/newmask2.png")
(srcr, srcg, srcb, srca) = new_img.split()
