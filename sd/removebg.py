
from rembg import remove, new_session
from PIL import Image


def sam_test():
    model_name = 'sam'
    session = new_session(model_name)

    image_path = "/Users/user/aigc/temp/test/rembg/temp/boys.webp"

    img = Image.open(image_path)

    result = remove(img, session=session, alpha_matting=False,
                    alpha_matting_foreground_threshold=200,
                    alpha_matting_background_threshold=130,
                    sam_prompt=[{"type": "point",
                                "data": [372, 529], "label": 1}, {"type": "point",
                                "data": [421, 385], "label": 1}, {"type": "point",
                                "data": [599, 322], "label": 1}, {"type": "point",
                                "data": [816, 244], "label": 1}, {"type": "point",
                                "data": [737, 211], "label": 0}]
                    )

    result.save("/Users/user/aigc/temp/test/rembg/temp/sam_13.png")
