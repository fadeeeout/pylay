from PIL import Image


def apply_mask(image_path, mask_path, output_path):
    # 打开原图和蒙版图像
    image = Image.open(image_path).convert("RGBA")
    mask = Image.open(mask_path)

    # 确保蒙版图像和原图像具有相同的尺寸
    mask = mask.resize(image.size, Image.LANCZOS)

    # 将蒙版图像转换为透明度通道
    mask = mask.convert("L")

    # 创建一个新的透明图像，将原图像复制到其中
    transparent_image = Image.new("RGBA", image.size)
    transparent_image.paste(image, (0, 0), mask=mask)

    # 保存结果图像
    transparent_image.save(output_path, "PNG")


def make_black_transparent(image_path):
    # 打开图像
    image = Image.open(image_path)

    # 将图像转换为带有 alpha 通道的模式
    image = image.convert("RGBA")

    # 获取图像的像素数据
    data = image.getdata()

    # 创建一个新的像素列表，将黑色部分的像素的 alpha 通道值设为 0
    new_data = []
    for item in data:
        # 如果像素为纯黑色 (R=0, G=0, B=0)，则将 alpha 通道设为 0
        if item[:3] == (0, 0, 0):
            new_data.append((0, 0, 0, 0))  # 黑色变为透明
        else:
            new_data.append(item)

    # 更新图像的像素数据
    image.putdata(new_data)
    return image


def make_white_transparent(image_path):
    # 打开图像
    image = Image.open(image_path)

    # 将图像转换为带有 alpha 通道的模式
    image = image.convert("RGBA")

    # 获取图像的像素数据
    data = image.getdata()

    # 创建一个新的像素列表，将黑色部分的像素的 alpha 通道值设为 0
    new_data = []
    for item in data:
        # 如果像素为纯黑色 (R=0, G=0, B=0)，则将 alpha 通道设为 0
        if item[:3] == (255, 255, 255):
            new_data.append((255, 255, 255, 0))  # 黑色变为透明
        else:
            new_data.append(item)

    # 更新图像的像素数据
    image.putdata(new_data)
    return image

def transfer(img_p):
    img = Image.open(img_p)
    img = img.convert('RGBA')
    data = img.getdata()
    new_data = []
    # 将所有透明度小于255的像素的透明度设为255
    for item in data:
        if item[3] < 255:
            new_data.append((item[0], item[1], item[2], 255))
        else:
            new_data.append(item)
    img.putdata(new_data)
    return img


if __name__ == '__main__':
    img_path = '/Users/user/aigc/temp/portrait.jpeg'
    mask_path = "/Users/user/aigc/temp/portrait_mask.png"
    output_path = "/Users/user/aigc/temp/masked5.png"
    # pmask = make_white_transparent(mask_path)
    # pmask.save(output_path, "PNG")
    # res = Image.open(img_path).convert("RGBA").crop(pmask.getbbox())
    # res.save(output_path, "PNG")
    apply_mask(img_path, mask_path, output_path)
    # transfer(img_path).save(output_path, "PNG")
    # Image.alpha_composite(Image.open(img_path).convert("RGBA"), make_black_transparent(mask_path)).save(output_path, "PNG")
