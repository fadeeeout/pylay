import requests

from util.helper import show_b64_img, image_to_base64, base64_to_np, save_b64_img


def mask_revert_payload(image_path: str) -> dict:
    img = image_to_base64(image_path)
    return {
        "image": img,
        "is_revert": True
    }


def mask_revert(payload: str, base_url: str) -> dict:
    url = f'{base_url}/service/v1/mask-convert'
    resp = requests.post(url=url, json=payload)
    if resp.status_code != 200:
        print(f'状态码：{resp.status_code}')
        print(f'错误信息：{resp.json()}')
        exit(1)
    return resp.json()


if __name__ == '__main__':
    image_file = "/Users/user/aigc/pic/bad_face.png"
    payload = mask_revert_payload(image_file)
    base_url = "http://127.0.0.1:7863"
    result = mask_revert(payload, base_url)
    if result['code'] != 200:
        print(result['message'])
        exit(1)
    mask = result['converted_image']
    save_b64_img(mask, "/Users/user/aigc/pic/test_mask_revert.png")
