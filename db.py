import sqlite3
import tiktoken

db_file = "/Users/user/work/temp/UOpenAI.db"

fetch_sql = "select content from message where content != '很抱歉，未得到AI的响应'"


def get_tokens_len(content):
    if content is None or content == '':
        return 0
    enc = tiktoken.encoding_for_model('gpt-3.5-turbo')
    tokens = enc.encode(content)
    return len(tokens)


def count_token():
    conn = sqlite3.connect(db_file)
    cursor = conn.cursor()
    all_rows = cursor.execute(fetch_sql).fetchall()
    token_lens = 0
    for row in all_rows:
        # print row's content
        if row is not None and row.__len__() > 0:
            content = row[0]
            token_lens += get_tokens_len(content)
    print("token_lens: ", token_lens)


if __name__ == '__main__':
    count_token()
