import numpy as np
from PIL import Image, ImageOps


def has_alpha(image):
    if image.getbands()[-1] != 'A':
        return False
    arr = np.array(image)
    return np.any(arr[:, :, 3] < 255)


def image_convert(image):
    if not has_alpha(image):
        raise Exception("蒙版没有透明区域")
    arr = np.array(image)
    # arr[:, :, 0:3] = np.where(arr[:, :, 3][:, :, None] > 0, [0, 0, 0], [255, 255, 255])
    new_data = np.empty(arr.shape, dtype=np.uint8)
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            alpha = arr[i, j, 3]
            new_data[i, j] = [255-alpha, 255-alpha, 255-alpha, alpha]
    new_img = Image.fromarray(new_data, mode="RGBA")
    return new_img


if __name__ == '__main__':
    image_path = "/Users/user/aigc/pic/manhua_ch_ori.png"
    image = Image.open(image_path)
    new_image = image_convert(image)
    new_image.save("/Users/user/aigc/pic/manhua_ch_ori_mask.png", "PNG")
    # image = Image.open(image_path)
    # image = image.convert('L')
    # crop_region = get_crop_region(np.array(image), 0)
    # print(crop_region)
    # crop_region = expand_crop_region(crop_region, 768, 512, image.width, image.height)
    # print(crop_region)
    # mask = image.crop(crop_region)
